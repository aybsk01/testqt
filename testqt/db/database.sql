PRAGMA encoding = "UTF-8";;


create table if not exists  providers (
    id integer primary key,
    json text
);;

create table if not exists  cards (
    id integer primary key,
    json text
);;
