#-------------------------------------------------
#
# Project created by QtCreator 2019-09-16T12:34:04
#
#-------------------------------------------------

QT       += core gui network sql qml quick quickcontrols2


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testqt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += src src/network src/data src/models

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        src/models/cardsfitermodel.cpp \
        src/network/networkaccessmanager.cpp \
        src/data/giftcard.cpp \
        src/data/dbconnector.cpp \
        src/data/provider.cpp \
        src/models/providersmodel.cpp \        
        src/models/giftcardsmodel.cpp \
        src/approot.cpp \

HEADERS += \
        src/models/cardsfitermodel.h \
        src/network/networkaccessmanager.h \
        src/data/giftcard.h \
        src/data/dbconnector.h \
        src/data/provider.h \
        src/models/providersmodel.h \    
        src/models/giftcardsmodel.h \
        src/approot.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
