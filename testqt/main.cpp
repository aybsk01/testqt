#include <QApplication>
#include <QQuickView>
#include <QQmlContext>

#include "approot.h"
#include "providersmodel.h"
#include "giftcardsmodel.h"
#include "cardsfitermodel.h"
#include "dbconnector.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    DbConnector::initializeDatabase();


    qmlRegisterType<AppRoot>("ru.test.approot", 1, 0, "AppRoot");
    qmlRegisterType<CardsFiterModel>("ru.test.filtercardsmodel", 1, 0, "CardsFiterModel");

    ProvidersModel providersModel;
    GiftCardsModel giftCardsModel;

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlContext *ctxt = view.rootContext();
    ctxt->setContextProperty("providersModel", &providersModel);
    ctxt->setContextProperty("giftCardsModel", &giftCardsModel);

    view.setSource(QUrl("qrc:/qml/main.qml"));
    view.show();

    return a.exec();
}
