import QtQuick 2.5

Item {
    id: root
    width: 200 * mainScale
    height : parent.height

    property alias header: headerItem.text
    property alias imgPath: cardImg.source
    property alias coinsVal: lbCoinsValue.text
    property bool invertColor : false

    signal clicked

    Rectangle {
        anchors.fill: parent
        anchors.margins: 10*mainScale
        anchors.topMargin: 5 * mainScale        
        color: "#f5f0f0"
        radius: 10

        Image{
            id : cardImg
            anchors.left: parent.left
            anchors.top : parent.top
            anchors.margins: 5 * mainScale
            width: parent.width * 0.4
            height: parent.height * 0.4
            fillMode: Image.PreserveAspectFit
        }

        Text {
            id: headerItem
            wrapMode: Text.Wrap
            anchors.right: parent.right
            anchors.top : parent.top
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            width: parent.width * 0.5
            height: parent.height * 0.5
            anchors.margins: 5 * mainScale
            color: "#5b666a"
        }

        Rectangle{
            radius: parent.radius
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height : parent.height * 0.4
            color : invertColor ? "#f9ae23" : "#5b666a"

            Rectangle{
                color : parent.color
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.radius
            }

            Row{
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 15 * mainScale
                anchors.right: parent.right
                anchors.margins: mainScale * 5
                spacing: 5 * mainScale
                Text{
                    id : lbCoinsValue
                    color: "white"
                    font.pixelSize: bigFont
                    horizontalAlignment: Text.AlignRight
                }
                Text{
                    id : lbCoinsCaption
                    color: "white"
                    font.pixelSize: bigFont/2
                    text : "coins"
                    height: lbCoinsValue.height
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }
    }
}
