import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Controls.Material 2.0

Item{    
    width: (parent == null) ? 10 : parent.width
    height: (parent == null) ? 10 : parent.height

    Rectangle{
        color : "#f0f5fa"
        anchors.fill: parent
    }

    ListView{
        id : lvProviders
        anchors.fill: parent
        anchors.margins: 15 * mainScale

        visible: !isLoading

        model : providersModel
        delegate: ProviderItemDelegate{
            header: title
            invertSubitemColor: invertCol
            providerId: provId
        }
    }
}
