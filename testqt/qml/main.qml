import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Controls.Material 2.0

import ru.test.approot 1.0

Item{

    property int defaultWidth: 480
    property int defaultHeight: 800
    width: 480
    height: 800
    property double mainScale: Math.min(Math.min(width, height) / defaultWidth, Math.max(width, height) / defaultHeight)
    property int bigFont : 32 * mainScale

    property bool isLoading : true

    Rectangle{
        color : "#f0f5fa"
        anchors.fill: parent
    }

    Text{
        visible: isLoading
        anchors.fill: parent
        font.pixelSize: bigFont
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text : "Выполняется загрузка данных..."
        color: "#5b666a"
    }

    Button{
        id : btnStartLoading
        text: qsTr("Reload data")
        height: mainScale * 50
        visible: false
        onClicked: {
            isLoading = true
            mainStackView.clear()
            appRoot.downloadProviders()
        }
    }

    AppRoot{
        id : appRoot
    }

    StackView{
        id : mainStackView
        anchors.top: btnStartLoading.bottom
        anchors.left: parent.left
        anchors.right:  parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 15 * mainScale
        visible: !isLoading
    }

    function providersLoadHandler(){
        isLoading = false
        mainStackView.push(Qt.resolvedUrl("ProvidersPage.qml"))
    }

    Component.onCompleted: {
        appRoot.downloadProviders()
        appRoot.providersLoaded.connect(providersLoadHandler)
    }


    Component.onDestruction: {
        appRoot.providersLoaded.disconnect(providersLoadHandler)
    }
}
