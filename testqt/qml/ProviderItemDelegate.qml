import QtQuick 2.5
import ru.test.filtercardsmodel 1.0

Item {
    id: root
    width: parent.width
    height: headerItem.height + lvCards.height + 35*mainScale

    property alias header: headerItem.text
    property bool invertSubitemColor : false
    property int providerId : -1

    CardsFiterModel{
        id : modelForFiter
        filterId: providerId
        sourceModel: giftCardsModel
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: 10*mainScale
        anchors.topMargin: 5 * mainScale        
        color: "#00FFFFFF"        
        radius: 1*mainScale


        Text {
            id: headerItem            
            font.pixelSize: 40 * mainScale
            wrapMode: Text.Wrap
            anchors.top: parent.top
            anchors.topMargin: 5*mainScale
            anchors.left: parent.left
            anchors.leftMargin: 20*mainScale
            anchors.right: parent.right
            anchors.rightMargin: 10*mainScale
        }

        ListView{
            id : lvCards
            anchors.top: headerItem.bottom
            anchors.topMargin: 5*mainScale
            anchors.left: parent.left
            anchors.leftMargin: 20*mainScale
            anchors.right: parent.right
            anchors.rightMargin: 10*mainScale
            height: 150 * mainScale
            model : modelForFiter
            orientation : ListView.Horizontal
            z : 10

            delegate: CardItemDelegate{
                header : title
                imgPath: imgUrl
                coinsVal: coins
                invertColor: invertSubitemColor
            }
        }
    }
}
