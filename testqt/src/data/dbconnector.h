#ifndef DBCONNECTOR_H
#define DBCONNECTOR_H

#include <QObject>
#include <QFile>
#include <QSqlDatabase>

#include "provider.h"
#include "giftcard.h"

class DbConnector : public QObject
{
    Q_OBJECT
signals:

public:
    explicit DbConnector(QObject *parent = nullptr);
    ~DbConnector();
    static bool initializeDatabase();
    bool saveProvidersAndCards(QVariant providersObj);

    QList<Provider> selectProviders();
    QList<GiftCard> selectGiftCards();

private:
    static const QString DB_CONN_NAME;
    QSqlDatabase db_;

    static bool checkAndCreate(const QString &path);
    static QString getDatabaseFileName();
    static QString getDatabasePath();

    bool createDatabase(const QString &fileName);
    QString getScript(const QString fileName);
    bool applyScript(const QString &sql);
    virtual QSqlDatabase &db() {return db_;}
    bool saveTable(QString tableName, const QVariantList &provs);


public slots:
};

#endif // DBCONNECTOR_H
