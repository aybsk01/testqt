#include "dbconnector.h"
#include <QFileInfo>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QDebug>
#include <QJsonDocument>

#include "provider.h"
#include "giftcard.h"

const QString DbConnector::DB_CONN_NAME = "ghTestConnection";

DbConnector::DbConnector(QObject *parent) : QObject(parent)
{
    QString dbName = getDatabaseFileName();

    if(QSqlDatabase::contains(DB_CONN_NAME)){
        db_ = QSqlDatabase::database(DB_CONN_NAME);
    }
    else
    {
        db_ = QSqlDatabase::addDatabase("QSQLITE", DB_CONN_NAME);
    }


    db_.setDatabaseName(dbName);
    if (!db_.isOpen() && !db_.open()){
        qWarning() << "Cant open database" << dbName;
    }
}

DbConnector::~DbConnector(){
    if(db_.isOpen())
        db_.close();
}

bool DbConnector::initializeDatabase()
{
    checkAndCreate(getDatabasePath());
    QString fileName = getDatabaseFileName().trimmed();
    if(fileName.isEmpty()) return  false;

    if(!QFile::exists(fileName))
    {
        return DbConnector().createDatabase(":/db/database.sql");
    }
    return true;
}

bool DbConnector::checkAndCreate(const QString &path)
{
    QFileInfo fi (path);
    if(!fi.exists())
    {
        QDir dir(fi.absoluteFilePath());
        QDir rd(dir.root());
        rd.mkpath(fi.absoluteFilePath());
    }
    fi = QFileInfo(path);
    return fi.exists();
}

bool DbConnector::createDatabase(const QString &fileName)
{
    QString sql = getScript(fileName);
    if(sql.isEmpty() || !(db_.isOpen() ||db_.open()))
    {
        QSqlError err = db_.lastError();
        qCritical() << (err.isValid() ? err.text() : tr("Db script is empty!"));
        return false;
    }

    return applyScript(sql);
}

bool DbConnector::applyScript(const QString &sql)
{
    bool res = db_.transaction();
    QStringList script = sql.split(";;");
    QSqlQuery q(db_);

    for(QString s: script)
    {
        if(s.size() < 7) continue;
        res = res && q.exec(s);
        if(!res)
        {
            s.replace("\r\n","\n");
            qCritical() << q.lastError();
            break;
        }
    }
    res = res && db_.commit();
    if(res)
    {
        return true;
    }
    qCritical() << db_.lastError();
    db_.rollback();
    return false;
}

QString DbConnector::getScript(const QString fileName)
{
    QFile f(fileName);
    if(!f.exists() || !f.open(QIODevice::ReadOnly))
    {
        qCritical() << tr("Can not open file %1 (%2)").arg(fileName).arg(f.errorString());
        return QString();
    }
    QTextStream ts(&f);
    ts.setCodec("UTF-8");
    QString res = ts.readAll();
    f.close();
    return res;
}

QString DbConnector::getDatabaseFileName()
{
    QString path = QString("%1/providers.db").arg(getDatabasePath());
    return path;
}

QString DbConnector::getDatabasePath()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::HomeLocation)+QDir::separator()+"providersdb"+QDir::separator();
    return path;
}

bool DbConnector::saveProvidersAndCards(QVariant providersObj){
    QVariantMap provMap = providersObj.toMap();
    if (!provMap.contains("providers")){
        return false;
    }

    QVariantList lst = provMap.value("providers").toList();
    if (lst.size() == 0){
        return false;
    }

    QVariantList providersForSave;
    QVariantList giftCardsForSave;

    foreach(QVariant provObj, lst){
        QVariantMap provMap = provObj.toMap();

        Provider provider = Provider::loadFromMap(provMap);
        QVariantMap provSaveMap;
        provSaveMap["id"] = provider.id();
        provSaveMap["json"] = QString::fromUtf8(QJsonDocument::fromVariant(provider.toMap()).toJson(QJsonDocument::Compact));
        providersForSave.append(provSaveMap);

        foreach(GiftCard card, provider.giftCards()){
            QVariantMap cardSaveMap;
            provSaveMap["id"] = card.id();
            provSaveMap["json"] = QString::fromUtf8(QJsonDocument::fromVariant(card.toMap()).toJson(QJsonDocument::Compact));
            giftCardsForSave.append(provSaveMap);
        }
    }
    bool res = saveTable("providers", providersForSave) && saveTable("cards", giftCardsForSave);
    return res;
}


bool DbConnector::saveTable(QString tableName, const QVariantList &values){
    if(db_.transaction())
    {
        QSqlQuery q(db_);
        q.prepare(QString("insert or replace into %1 (id, json) values (:id, :json)").arg(tableName));
        for(const QVariant &v :values)
        {
            QVariantMap m = v.toMap();
            q.bindValue(":id", m["id"]);
            q.bindValue(":json", m["json"]);
            if(!q.exec())
            {
                qWarning() << q.lastError() << q.executedQuery();
            }
        }
        if(!db_.commit())
        {
            qWarning() << db_.lastError();
            db_.rollback();
            return false;
        }
        return true;
    }
    return false;
}

QList<Provider> DbConnector::selectProviders(){
    QList<Provider> res;

    if(db_.transaction())
    {
        QSqlQuery q(db_);
        QString sql = "select * from providers";

        if(!q.exec(sql))
        {
            qWarning() << q.lastError();
        }
        else
        {
            while (q.next())
            {
                Provider p = Provider::loadFromMap(QJsonDocument::fromJson(q.value("json").toString().toUtf8()).toVariant().toMap());
                p.setId(q.value("id").toInt());
                res.append(p);
            }
        }

        db_.commit() || db_.rollback();
    }
    return res;
}

QList<GiftCard> DbConnector::selectGiftCards(){
    QList<GiftCard> res;

    if(db_.transaction())
    {
        QSqlQuery q(db_);
        QString sql = "select * from cards";

        if(!q.exec(sql))
        {
            qWarning() << q.lastError();
        }
        else
        {
            while (q.next())
            {
                GiftCard card = GiftCard::loadFromMap(QJsonDocument::fromJson(q.value("json").toString().toUtf8()).toVariant().toMap());
                card.setId(q.value("id").toInt());
                res.append(card);
            }
        }

        db_.commit() || db_.rollback();
    }

    return res;
}
