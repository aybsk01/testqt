#ifndef GIFTCARD_H
#define GIFTCARD_H

#include <QString>
#include <QVariant>

class GiftCard
{
public:
    GiftCard();

    int id() const;
    void setId(int id);

    int credits() const;
    void setCredits(int credits);

    int codesCount() const;
    void setCodesCount(int codesCount);

    bool featured() const;
    void setFeatured(bool featured);

    QString imageUrl() const;
    void setImageUrl(const QString &imageUrl);

    QString title() const;
    void setTitle(const QString &title);

    QString currency() const;
    void setCurrency(const QString &currency);

    QString description() const;
    void setDescription(const QString &description);

    QString redeem_url() const;
    void setRedeem_url(const QString &redeem_url);

    static GiftCard loadFromMap(QVariantMap map);
    QVariantMap toMap(bool compact = true);

    int providerId() const;
    void setProviderId(int providerId);

private:
    int id_;
    int providerId_;
    int credits_;
    int codesCount_;
    bool featured_;
    QString imageUrl_;
    QString title_;
    QString currency_;
    QString description_;
    QString redeem_url_;
};

#endif // GIFTCARD_H
