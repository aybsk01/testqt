#ifndef PROVIDER_H
#define PROVIDER_H

#include <QString>
#include <QList>
#include "giftcard.h"

class Provider
{
public:
    Provider();    
    int id() const;
    void setId(int id);

    QString title() const;
    void setTitle(const QString &title);

    QString imageUrl() const;
    void setImageUrl(const QString &imageUrl);

    QList<GiftCard> giftCards() const;
    void setGiftCards(const QList<GiftCard> &giftCards);

    static Provider loadFromMap(QVariantMap map);
    QVariantMap toMap(bool compact = true);

private:
    int id_;
    QString title_;
    QString imageUrl_;
    QList<GiftCard> giftCards_;
};

#endif // PROVIDER_H
