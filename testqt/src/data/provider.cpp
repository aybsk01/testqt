#include "provider.h"

Provider::Provider():
    id_(0),
    title_(QString()),
    imageUrl_(QString())
{

}

int Provider::id() const
{
    return id_;
}

void Provider::setId(int id)
{
    id_ = id;
}

QString Provider::title() const
{
    return title_;
}

void Provider::setTitle(const QString &title)
{
    title_ = title;
}

QString Provider::imageUrl() const
{
    return imageUrl_;
}

void Provider::setImageUrl(const QString &imageUrl)
{
    imageUrl_ = imageUrl;
}

QList<GiftCard> Provider::giftCards() const
{
    return giftCards_;
}

void Provider::setGiftCards(const QList<GiftCard> &giftCards)
{
    giftCards_ = giftCards;
}

Provider Provider::loadFromMap(QVariantMap map){
    Provider res;
    if (map.contains("id")){
        res.setId(map.value("id").toInt());
    }
    if (map.contains("title")){
        res.setTitle(map.value("title").toString());
    }
    if (map.contains("image_url")){
        res.setImageUrl(map.value("image_url").toString());
    }
    if (map.contains("gift_cards")){
        QVariantList cards = map.value("gift_cards").toList();
        QList<GiftCard> giftCards;
        foreach(QVariant card, cards){
            GiftCard giftCard = GiftCard::loadFromMap(card.toMap());
            giftCard.setProviderId(res.id());
            giftCards.append(giftCard);
        }
        res.setGiftCards(giftCards);
    }

    return res;
}

QVariantMap Provider::toMap(bool compact){
    QVariantMap res;

    if (compact){
        res["title"] = title_;
        res["image_url"] = imageUrl_;
        QVariantList cardsIds;
        foreach(GiftCard card, giftCards_){
            cardsIds.append(card.id());
        }
        res["gift_cards"] = cardsIds;
    }

    return res;
}
