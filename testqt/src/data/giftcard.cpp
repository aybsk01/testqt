#include "giftcard.h"

GiftCard::GiftCard()
{

}

int GiftCard::id() const
{
    return id_;
}

void GiftCard::setId(int id)
{
    id_ = id;
}

int GiftCard::credits() const
{
    return credits_;
}

void GiftCard::setCredits(int credits)
{
    credits_ = credits;
}

int GiftCard::codesCount() const
{
    return codesCount_;
}

void GiftCard::setCodesCount(int codesCount)
{
    codesCount_ = codesCount;
}

bool GiftCard::featured() const
{
    return featured_;
}

void GiftCard::setFeatured(bool featured)
{
    featured_ = featured;
}

QString GiftCard::imageUrl() const
{
    return imageUrl_;
}

void GiftCard::setImageUrl(const QString &imageUrl)
{
    imageUrl_ = imageUrl;
}

QString GiftCard::title() const
{
    return title_;
}

void GiftCard::setTitle(const QString &title)
{
    title_ = title;
}

QString GiftCard::currency() const
{
    return currency_;
}

void GiftCard::setCurrency(const QString &currency)
{
    currency_ = currency;
}

QString GiftCard::description() const
{
    return description_;
}

void GiftCard::setDescription(const QString &description)
{
    description_ = description;
}

QString GiftCard::redeem_url() const
{
    return redeem_url_;
}

void GiftCard::setRedeem_url(const QString &redeem_url)
{
    redeem_url_ = redeem_url;
}



GiftCard GiftCard::loadFromMap(QVariantMap map){
    GiftCard res;
    if (map.contains("id")){
        res.setId(map.value("id").toInt());
    }
    if (map.contains("providerId")){
        res.setProviderId(map.value("providerId").toInt());
    }
    if (map.contains("featured")){
        res.setFeatured(map.value("featured").toBool());
    }
    if (map.contains("title")){
        res.setTitle(map.value("title").toString());
    }
    if (map.contains("credits")){
        res.setCredits(map.value("credits").toInt());
    }
    if (map.contains("image_url")){
        res.setImageUrl(map.value("image_url").toString());
    }
    if (map.contains("codes_count")){
        res.setCodesCount(map.value("codes_count").toInt());
    }
    if (map.contains("currency")){
        res.setCurrency(map.value("currency").toString());
    }
    if (map.contains("description")){
        res.setDescription(map.value("description").toString());
    }
    if (map.contains("redeem_url")){
        res.setRedeem_url(map.value("redeem_url").toString());
    }
    return res;
}

QVariantMap GiftCard::toMap(bool compact){
    QVariantMap res;

    if (compact){
        res["providerId"] = providerId_;
        res["featured"] = featured_;
        res["title"] = title_;
        res["credits"] = credits_;
        res["image_url"] = imageUrl_;
        res["codes_count"] = codesCount_;
        res["currency"] = currency_;
        res["description"] = description_;
        res["redeem_url"] = redeem_url_;
    }

    return res;
}

int GiftCard::providerId() const
{
    return providerId_;
}

void GiftCard::setProviderId(int providerId)
{
    providerId_ = providerId;
}
