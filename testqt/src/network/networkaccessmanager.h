#ifndef NETWORKACCESSMANAGER_H
#define NETWORKACCESSMANAGER_H

#include <QObject>
#include <QPointer>
#include <QNetworkRequest>


class QNetworkAccessManager;
class QNetworkReply;

class NetworkAccessManager : public QObject
{
    Q_OBJECT

    static const QString SERVER_HOST;
    static const int SERVER_PORT;
    static const QString GET_PROVIDERS_PATH;

signals:
    void error(int errorCode, QString errorMsg);
    void providersLoaded();

public:
    enum Error: int
    {
        ERROR_NO_ERRORS					= 0x0000,
        ERROR_NO_REPLY              	= 0x0001,
        ERROR_NO_CONTENT              	= 0x0002,
    };

    explicit NetworkAccessManager(QObject *parent = nullptr);
    void downloadProviders();

private:
    QString serverHost_;
    int serverPort_;
    QPointer<QNetworkAccessManager> http_;
    QNetworkAccessManager* newAccessMgr();
    QNetworkRequest prepareRequest(const QString &path, QNetworkAccessManager *accessMgr, const QUrlQuery &query);
    bool prepareReply(QNetworkReply *reply);

public slots:

private slots:
    void finished(QNetworkReply* reply);
};

#endif // NETWORKACCESSMANAGER_H
