#include "networkaccessmanager.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QUrl>
#include <QJsonDocument>

#include "dbconnector.h"

const QString NetworkAccessManager::SERVER_HOST = "91.240.86.243";
const int NetworkAccessManager::SERVER_PORT = 8883;
const QString NetworkAccessManager::GET_PROVIDERS_PATH = "/files/providers.json";

NetworkAccessManager::NetworkAccessManager(QObject *parent) :
    QObject(parent),
    serverHost_(SERVER_HOST),
    serverPort_(SERVER_PORT)
{

}

QNetworkAccessManager* NetworkAccessManager::newAccessMgr()
{
    if(http_) return http_;
    QNetworkAccessManager* mgr = new QNetworkAccessManager(this);
    connect(mgr, SIGNAL(finished(QNetworkReply*)), SLOT(finished(QNetworkReply*)));
    http_ = mgr;
    return mgr;
}

void NetworkAccessManager::finished(QNetworkReply *reply)
{
    if(!reply)
    {
        qCritical() << tr("Reply is null object");
        if(sender())
        {
            if(sender() == http_) http_ = nullptr;
            sender()->blockSignals(true);
            sender()->deleteLater();
        }
        emit error(Error::ERROR_NO_REPLY, tr("Internal reply error"));
        return;
    }

    prepareReply(reply);
    reply->close();
    reply->deleteLater();
}

QNetworkRequest NetworkAccessManager::prepareRequest(const QString &path,QNetworkAccessManager *accessMgr, const QUrlQuery &query)
{
    QUrl url;
    url.setHost(serverHost_);
    url.setPath(path);
    url.setPort(serverPort_);
    url.setScheme("http");
    QNetworkRequest req(url);
    return req;
}

void NetworkAccessManager::downloadProviders()
{

    QNetworkAccessManager* accessMgr = newAccessMgr();
    QNetworkRequest req = prepareRequest(GET_PROVIDERS_PATH, accessMgr, QUrlQuery());
    qDebug() << req.url().toString();
    QNetworkReply* reply = accessMgr->get(req);
    if (!reply){
        emit error(ERROR_NO_REPLY, tr("No reply"));
    }
}

bool NetworkAccessManager::prepareReply(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
    {
        emit error(ERROR_NO_REPLY, tr("Reply error %1").arg(reply->errorString()));
        return false;
    }

    QJsonParseError jerr;
    QByteArray ansrepl = reply->readAll();

    QJsonDocument jdoc = QJsonDocument::fromJson(ansrepl, &jerr);
    if(jerr.error != QJsonParseError::NoError)
    {
        QString serr = jerr.errorString();
        jdoc = QJsonDocument();
        emit error(ERROR_NO_CONTENT, tr("Answer format data error"));
        return false;
    }

    DbConnector().saveProvidersAndCards(jdoc.toVariant());
    emit providersLoaded();
    return true;
}
