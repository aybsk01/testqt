#ifndef APPROOT_H
#define APPROOT_H

#include <QObject>
#include <QPointer>

class NetworkAccessManager;

class AppRoot : public QObject
{
    Q_OBJECT

signals:
    void providersLoaded();
    void loadError(int code, QString msg);

public:
    explicit AppRoot(QObject *parent = nullptr);

    Q_INVOKABLE void downloadProviders();

private:
    QPointer<NetworkAccessManager> networkManager_;



public slots:
};

#endif // APPROOT_H
