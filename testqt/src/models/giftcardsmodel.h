#ifndef GIFTCARDSMODEL_H
#define GIFTCARDSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "giftcard.h"

class GiftCardsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum GiftCardsModelRoles {
        TitleRole = Qt::UserRole + 1,
        ImageRole = Qt::UserRole + 2,
        CoinsRole = Qt::UserRole + 3,
        ProviderIdRole = Qt::UserRole + 4,
        SizeRole
    };

    explicit GiftCardsModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex & parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;
    bool canFetchMore(const QModelIndex &parent) const override;
    void fetchMore(const QModelIndex &parent) override;

private:
    QList<GiftCard> giftCards_;


signals:

public slots:
};

#endif // GiftCardsModel_H
