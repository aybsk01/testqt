#ifndef PROVIDERSMODEL_H
#define PROVIDERSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "provider.h"

class ProvidersModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ProvidersModelRoles {
        TytleRole = Qt::UserRole + 1,
        InvertColorRole = Qt::UserRole + 2,
        ProviderIdRole = Qt::UserRole + 3,
        SizeRole
    };


    explicit ProvidersModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex & parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    //Q_INVOKABLE void reloadProviders();


protected:
    QHash<int, QByteArray> roleNames() const override;
    bool canFetchMore(const QModelIndex &parent) const override;
    void fetchMore(const QModelIndex &parent) override;

private:
    QList<Provider> providers_;


signals:

public slots:
};

#endif // PROVIDERSMODEL_H
