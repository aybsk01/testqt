#include "giftcardsmodel.h"
#include "dbconnector.h"

GiftCardsModel::GiftCardsModel(QObject *parent) : QAbstractListModel(parent)
{

}

int GiftCardsModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent)
    return giftCards_.count();
}

QVariant GiftCardsModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= giftCards_.count())
        return QVariant();

    GiftCard card = giftCards_.at(index.row());

    if (role == TitleRole)
        return card.title();
    else
    if (role == ImageRole)
        return card.imageUrl();
    else
    if (role == CoinsRole)
        return card.credits();
    else
    if (role == ProviderIdRole)
        return card.providerId();
    else
    if (role == SizeRole)
        return giftCards_.size();
    return QVariant();
}

bool GiftCardsModel::canFetchMore(const QModelIndex &parent) const
{
    if (parent.isValid())
        return false;

    //Тут должно быть что то более оптимальное, это вариант для теста
    //То есть оценить, что в базе есть еще записи, не загруженные в модель
    DbConnector dbc;
    bool can = giftCards_.size() < dbc.selectGiftCards().size();
    return can;
}

//И фетчим их
void GiftCardsModel::fetchMore(const QModelIndex &parent)
{
    if (parent.isValid())
        return;

    DbConnector dbc;
    int currentSize = giftCards_.size();
    int newSize = dbc.selectGiftCards().size();

    beginInsertRows(QModelIndex(), currentSize, newSize - 1);
    giftCards_ = dbc.selectGiftCards();
    endInsertRows();
}


QHash<int, QByteArray> GiftCardsModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[TitleRole] = "title";
    roles[ImageRole] = "imgUrl";
    roles[ProviderIdRole] = "providerId";
    roles[CoinsRole] = "coins";
    roles[SizeRole] = "size";    
    return roles;
}
