#ifndef CARDSFITERMODEL_H
#define CARDSFITERMODEL_H

#include <QSortFilterProxyModel>


class CardsFiterModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int filterId READ filterId WRITE setFilterId)

public:
    explicit CardsFiterModel(QObject *parent = nullptr);

    Q_INVOKABLE int filterId() const;
    Q_INVOKABLE void setFilterId(int filterId);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    int filterId_;

signals:

public slots:
};

#endif // CARDSFITERMODEL_H
