#include "cardsfitermodel.h"
#include <QDebug>

CardsFiterModel::CardsFiterModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    filterId_(-1)
{

}

bool CardsFiterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (filterId_ == -1){
        return true;
    }
    qDebug() <<  sourceParent.data(Qt::UserRole + 1).toString();
    qDebug() <<  filterRole() << filterId_;
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    int indexProviderId =  sourceModel()->data(index0, Qt::UserRole +4).toInt();
    return (filterId_ == indexProviderId);
}

int CardsFiterModel::filterId() const
{
    return filterId_;
}

void CardsFiterModel::setFilterId(int filterId)
{
    filterId_ = filterId;    
}
