#include "providersmodel.h"
#include "dbconnector.h"

ProvidersModel::ProvidersModel(QObject *parent) : QAbstractListModel(parent)
{

}

int ProvidersModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent)
    return providers_.count();
}

QVariant ProvidersModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= providers_.count())
        return QVariant();

    Provider p = providers_.at(index.row());

    if (role == TytleRole)
        return p.title();
    else
    if (role == SizeRole)
        return providers_.size();
    else
    if (role == ProviderIdRole)
        return p.id();
    else
    if (role == InvertColorRole)
        return (index.row() % 2 != 0);
    return QVariant();
}


QHash<int, QByteArray> ProvidersModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[TytleRole] = "title";
    roles[SizeRole] = "size";
    roles[InvertColorRole] = "invertCol";
    roles[ProviderIdRole] = "provId";
    return roles;
}

bool ProvidersModel::canFetchMore(const QModelIndex &parent) const
{
    if (parent.isValid())
        return false;

    //Тут должно быть что то более оптимальное, это вариант для теста
    //То есть оценить, что в базе есть еще записи, не загруженные в модель
    DbConnector dbc;
    bool can = providers_.size() < dbc.selectProviders().size();
    return can;
}

//И фетчим их
void ProvidersModel::fetchMore(const QModelIndex &parent)
{
    if (parent.isValid())
        return;

    DbConnector dbc;
    int currentSize = providers_.size();
    int newSize = dbc.selectProviders().size();

    beginInsertRows(QModelIndex(), currentSize, newSize - 1);
    providers_ = dbc.selectProviders();
    endInsertRows();
}
