#include "approot.h"
#include "networkaccessmanager.h"


AppRoot::AppRoot(QObject *parent) :
    QObject(parent),
    networkManager_(new NetworkAccessManager(this))
{
    connect(networkManager_.data(), SIGNAL(providersLoaded()), this, SIGNAL(providersLoaded()));
    connect(networkManager_.data(), SIGNAL(error(int, QString)), this, SIGNAL(loadError(int, QString)));
}

void AppRoot::downloadProviders(){
    if (networkManager_){
        networkManager_->downloadProviders();
    }
}
